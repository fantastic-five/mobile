import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import RootNavigation from './navigation/RootNavigation';

import { Permissions } from 'expo';

export default class App extends React.Component {
	state = {
		isLoadingComplete: false
	};
	
	async componentWillMount() {
		let cameraStatus = {
			status: false
		};
		
		let locationStatus = {
			status: false
		};
		
		do {
			locationStatus = await Permissions.askAsync(Permissions.LOCATION);
		} while (locationStatus.status !== 'granted');
		
		do {
			cameraStatus = await Permissions.askAsync(Permissions.CAMERA);
		} while (cameraStatus.status !== 'granted');
	}
	
	render() {
		console.disableYellowBox = true;
		
		if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
			return (
				<AppLoading
					startAsync={this._loadResourcesAsync}
					onError={this._handleLoadingError}
					onFinish={this._handleFinishLoading}
				/>
			);
		} else {
			return (
				<View style={styles.container}>
					{Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
					<RootNavigation/>
				</View>
			);
		}
	}
	
	_loadResourcesAsync = async () => {
		return Promise.all([
			Asset.loadAsync([
				require('./assets/images/icon.png'),
				require('./assets/images/loading.gif'),
				require('./assets/images/splash.png'),
				require('./assets/images/categories/ambrosia.jpg'),
				require('./assets/images/categories/default.jpg'),
				require('./assets/images/categories/potholes.jpg'),
				require('./assets/images/categories/trash.jpg')
			]),
			Font.loadAsync({
				...Ionicons.font,
				'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')
			})
		]);
	};
	
	_handleLoadingError = error => {
		// In this case, you might want to report the error to your error
		// reporting service, for example Sentry
		console.warn(error);
	};
	
	_handleFinishLoading = () => {
		this.setState({isLoadingComplete: true});
	};
}

const styles = StyleSheet.create({
	container: {
		flex           : 1,
		backgroundColor: '#fff'
	}
});
