import React from 'react';
import {
	Image,
	StyleSheet,
	Text,
	TextInput,
	View,
	Alert
} from 'react-native';

import { Button, Card, Icon } from 'react-native-elements';

export default class ReportScreen extends React.Component {
	static navigationOptions = {
		header: null
	};
	
	state = {
		loading: false,
		comment: ''
	};
	
	async _reportIssue() {
		const { navigation } = this.props;
		const data = navigation.getParam('data');

		try {
			let response = await fetch(
				'https://shamyamy-api.herokuapp.com/api/issue', {
				//'https://jsontestamy.herokuapp.com/w8r4qow8', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					id: data.id,
					category: data.category,
					description: this.state.comment,
				}),
			});
		
			if (response.ok) {
				Alert.alert("Thanks for reporting", "Done, I notified the authorities.");
				
				navigation.navigate('Home');
			} else {
				Alert.alert("Uuups", "Something went wrong. Please try again.");
			}
			
		} catch (error) {
			Alert.alert("Uuups", "Something went wrong. Please try again.");
			
			console.error(error);
		}
	}

	render() {
		let image, message;
		const { navigation } = this.props;
		const data = navigation.getParam('data');
		let detected = true;
    
		switch (data.category) {
			case 'ambrosia' : 
				image = require('../assets/images/categories/ambrosia.jpg');
				message = "We detected ambrosia, is this correct? If so let\'s report it.";
				break;
			case 'trash' : 
				image = require('../assets/images/categories/trash.jpg');
				message = "We detected trash, is this correct? If so let\'s report it.";
				break;	
			case 'pothole' :
				image = require('../assets/images/categories/potholes.jpg');
				message = "We detected a pot hole, is this correct? If so let\'s report it.";
				break;	
			default : 
				image = require('../assets/images/categories/default.jpg');
				message = "We didn\'t detect anything.";
				detected = false;
				break;
		} 

		if (detected) {
			return (
				<View style={styles.container}>					
					<Card
						title='Here is what I identified.'
						image={image}>
	
						<Text style={{marginBottom: 10}}>
							{message}
						</Text>

						<TextInput
							multiline={true}
							style={{height: 60, borderColor: '#dfdfe6', borderWidth: 1, padding: 10, marginBottom: 20}}
							numberOfLines={4}
							placeholder="Add your comment"
    						placeholderTextColor="#dfdfe6"
							editable = {true}
							onChangeText={(comment) => this.setState({comment})}
							value={this.state.comment} />
						
						<Button
							icon={<Icon name='code' color='#ffffff' />}
							backgroundColor='#03A9F4'
							buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
							title='Report'
						 	onPress={this._reportIssue.bind(this)}/>
					</Card>
				</View>
			);
		} else {
			return (
				<View style={styles.container}>
					<Card
						title={message}
						image={image}
						style={styles.cardSize}
						>

						<Button
							icon={<Icon name='code' color='#ffffff' />}
							backgroundColor='#03A9F4'
							buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
							title='Try Again'
							onPress={event => {
								this.props.navigation.navigate('Home');
								return false;
							}}
							/>
					</Card>
				</View>
			);
		}
	}
}

const styles = StyleSheet.create({	
	container: {
		flex: 1,
		paddingTop: 40,
		backgroundColor: '#fff'
	},
	cardSize: {
		height: 500
	}

});
