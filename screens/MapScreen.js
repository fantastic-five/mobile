import React from 'react';

import {
	StyleSheet,
	View,
	WebView
} from 'react-native';

import { Location } from 'expo';

import MapView, { Marker } from 'react-native-maps';

export default class MapScreen extends React.Component {
	static navigationOptions = {
		title: 'Map'
	};
	
	state = {
		location: null,
		points: []
	};

	async componentWillMount() {
		let location = await Location.getCurrentPositionAsync({});
		this.setState({location});
		
		let rawPoints = await fetch('https://shamyamy-api.herokuapp.com/api/getAllPoints');
		let points    = await rawPoints.json();
		
		this.setState({
			points: points.reports
		});
	}
	
	render() {
		
		return (
			<WebView
				source={{uri: 'http://fantastic-five.gitlab.io/web/?noSidebar'}}
				style={styles.container}
			/>
		);
		
		
		
		//const {location, points} = this.state;
		//
		//if (!location) {
		//	return <View/>;
		//} else {
		//	return (
		//		<View style={styles.container}>
		//			<MapView
		//				style={styles.map}
		//				region={{
		//					latitude      : location.coords.latitude,
		//					longitude     : location.coords.longitude,
		//					latitudeDelta : 0.09,
		//					longitudeDelta: 0.0121
		//				}}
		//			>
		//				{points.map(point => (
		//					<Marker
		//						coordinate={{
		//							latitude : point.coords.lat,
		//							longitude: point.coords.long
		//						}}
		//						title={point.category}
		//						description={point.description}
		//					/>
		//				))}
		//			</MapView>
		//
		//		</View>
		//	);
		//}
	}
}

const styles = StyleSheet.create({
	container: {
		...StyleSheet.absoluteFillObject,
		justifyContent: 'flex-end',
		alignItems    : 'center'
	},
	map      : {
		...StyleSheet.absoluteFillObject
	}
});