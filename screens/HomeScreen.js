import React from 'react';
import {
	Image,
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
	Modal,
	ActivityIndicator
} from 'react-native';

import { Button } from 'react-native-elements';

import { Camera, Permissions, Location } from 'expo';

import { MonoText } from '../components/StyledText';

const Loader = props => {
	const {
	  loading,
	  ...attributes
	} = props;
  
	return (
	  <Modal
		transparent={true}
		animationType={'none'}
		visible={loading}
		onRequestClose={() => {console.log('close modal')}}>
		<View style={styles.modalBackground}>
		  <View style={styles.activityIndicatorWrapper}>
			<Image
				resizeMethod='auto'
				style={{width: 50, height: 50}}
				source={require('../assets/images/loading.gif')}
			/>
			<Text>Thinking</Text>
		  </View>
		</View>
	  </Modal>
	)
  };

export default class HomeScreen extends React.Component {
	static navigationOptions = {
		header: null
	};
	
	camera;
	
	state = {
		location: null,
		loading: false,
		type : Camera.Constants.Type.back
	};
	
	async componentWillMount() {
		let location = await Location.getCurrentPositionAsync({});
		this.setState({ location });
	}
	
	async _sendImage() {

		const { navigate } = this.props.navigation;

		if (this.camera) {
			let photo = await this.camera.takePictureAsync({
				quality: 0.8,
				exif   : true
			});

			this.setState({
				loading: true
			});
			
			if (photo) {
				const data = new FormData();
				data.append('photo', {
					uri : photo.uri,
					name: 'report_image.jpg',
					type: 'image/jpg'
				});

				data.append('photoExif', JSON.stringify(photo.exif));
				data.append('location', JSON.stringify({
					latitude: this.state.location.coords.latitude,
              		longitude: this.state.location.coords.longitude,
				}));

				const config = {
					method : 'POST',
					headers: {
						'Accept'       : 'application/json',
						'Content-Type' : 'multipart/form-data;'
					},
					body   : data
				};
				
				try {
					const responseData = await fetch('https://shamyamy-api.herokuapp.com/api/upload', config);
					const data = await responseData.json();

					navigate('Report', { 
						data
					})

					this.setState({
						loading: false
					});

				} catch (e) {
					console.log(err);
					alert (err.message);

					this.setState({
						loading: false
					});
				}
			}
		}
	}
	
	render() {
		return (
			<View style={styles.container}>
				<Loader loading={this.state.loading} />
				<Camera style={{flex: 1, marginBottom: 110}} type={this.state.type} ref={ref => {
					this.camera = ref;
				}}>
				</Camera>
				
				<View style={styles.tabBarInfoContainer}>
					<Button
						icon={{
							name : 'camera',
							size : 15,
							color: 'white'
						}}
						backgroundColor='#03A9F4'
						style={styles.buttonCamera}
						title="Report Issue" onPress={this._sendImage.bind(this)}> </Button>
					
					<Text style={styles.tabBarInfoText}>In order for us to identify the issue you have to take a
						picture.</Text>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	modalBackground: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'space-around',
		backgroundColor: '#00000040'
	},
	buttonCamera: {
		borderRadius: 10
	},
	activityIndicatorWrapper: {
		backgroundColor: '#FFFFFF',
		height: 100,
		width: 100,
		borderRadius: 10,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-around'
	},
	container             : {
		flex           : 1,
		backgroundColor: '#fff'
	},
	tabBarInfoContainer   : {
		position       : 'absolute',
		bottom         : 0,
		left           : 0,
		right          : 0,
		...Platform.select({
			ios    : {
				shadowColor  : 'black',
				shadowOffset : {height: -3},
				shadowOpacity: 0.1,
				shadowRadius : 3
			},
			android: {
				elevation: 20
			}
		}),
		alignItems     : 'center',
		backgroundColor: '#fbfbfb',
		paddingVertical: 20
	},
	tabBarInfoText        : {
		fontSize : 17,
		marginTop: 10,
		color    : 'rgba(96,100,109, 1)',
		textAlign: 'center'
	}
});
