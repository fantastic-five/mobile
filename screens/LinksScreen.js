import React from 'react';
import { Image, ListView, RefreshControl, ScrollView, StyleSheet, View } from 'react-native';

import { Card, Text} from 'react-native-elements';

export default class LinksScreen extends React.Component {
	static navigationOptions = {
		title: 'Reported Issues'
	};
	
	state = {
		issues: [],
		refreshing: false
	};
	
	async fetchData() {
		let rawIssues = await fetch('https://shamyamy-api.herokuapp.com/api/getAllPoints');
		let issues = await rawIssues.json();
		
		return issues.reports;
	}
	
	async componentWillMount() {
		const data = await this.fetchData();
		
		this.setState({
			issues: data
		});
	}
	
	async _onRefresh() {
		this.setState({refreshing: true});
		
		const data = await this.fetchData();
		
		this.setState({
			issues: data,
			refreshing: false
		});
	}
	
	render() {
		const {issues} = this.state;
		
		return (
			<ScrollView
				refreshControl={
					<RefreshControl
						refreshing={this.state.refreshing}
						onRefresh={this._onRefresh.bind(this)}
					/>
				}
				style={styles.container}>
					{
						issues.map((item, i)=>{
							let cardStyle;
							
							switch (item.category) {
								case 'ambrosia':
									cardStyle = styles.borderAmbrosia;
									break;
								case 'pothole':
									cardStyle = styles.borderPothole;
									break;
								case 'trash':
									cardStyle = styles.borderTrash;
									break;
								default:
									cardStyle = styles.cardStyle;
							}
							
							return (
								<View key={i} style={ cardStyle }>
									<Image
										style={styles.image}
										resizeMode="cover"
										source={{ uri: item.thumbnailImg }}
									/>
									<View>
										<Text style={styles.text}> You found: </Text>
										<Text style={styles.name}> {item.category}</Text>
										<Text style={styles.text}> Status: </Text>
										<Text style={styles.name}> Email sent to Retim</Text>
									</View>
									
								</View>
							)
						})
					}
			</ScrollView>
		);
	}
}


const styles = StyleSheet.create({
	container: {
		flex           : 1,
		padding        : 20,
		paddingBottom: 50,
		backgroundColor: '#fff'
	},
	cardStyle: {
		borderRadius: 8,
		flex           : 1,
		flexDirection: 'row',
		borderWidth: 1,
		padding: 7,
		borderColor: '#bdb9bc',
		marginBottom: 15
	},
	image: {
		width: 150,
		borderRadius: 6,
		height: 150
	},
	text: {
	
	},
	borderAmbrosia: {
		borderRadius: 8,
		flex           : 1,
		flexDirection: 'row',
		borderWidth: 1,
		padding: 7,
		marginBottom: 15,
		borderColor: '#378e36',
	},
	borderTrash: {
		borderRadius: 8,
		flex           : 1,
		flexDirection: 'row',
		borderWidth: 1,
		padding: 7,
		marginBottom: 15,
		borderColor: '#6e3f19',
	},
	borderPothole: {
		borderRadius: 8,
		flex           : 1,
		flexDirection: 'row',
		borderWidth: 1,
		padding: 7,
		marginBottom: 15,
		borderColor: '#979396',
	},
	name: {
		paddingLeft: 15,
		paddingTop: 8,
		fontSize: 18,
		marginBottom: 20,
		fontWeight: 'bold'
	}
});
